import { LinkProps } from "@chakra-ui/react";
import { ReactElement } from "react";

export interface LinkButtonProps extends LinkProps {
  text: string;
  rightIcon?: ReactElement;
  leftIcon?: ReactElement;
}
