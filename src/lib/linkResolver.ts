export const linkResolver = (link: string) => {
  try {
    if (!process.env.WORDPRESS_API_URL) return link;
    const { pathname, host } = new URL(link);

    // if external
    const { host: host2 } = new URL(process.env.WORDPRESS_API_URL);
    if (host !== host2) return link;

    console.log(process.env.NEXT_PUBLIC_APP_URL);

    return process.env.NEXT_PUBLIC_APP_URL + pathname;
  } catch (e) {
    console.error(
      "WARNING - LinkResolver - Failed: ",
      link,
      " - Error: ",
      JSON.stringify(e)
    );
    return link;
  }
};
