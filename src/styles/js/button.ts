import { fonts } from "./fonts";

export const Button = {
  // 1. Update the base styles
  baseStyle: {
    borderRadius: "base",
    fontFamily: fonts.body,
    fontWeight: 700,
    lineHeight: "1.5em",
    letterSpacing: -0.16,
    color: "abyss.800",
    _disabled: {
      color: "granite.300",
    },
  },
  sizes: {
    md: {
      h: "3em",
      fontSize: "md",
      px: "22px",
      py: "12px",
    },
  },
  defaultProps: {
    variant: "solid",
    size: "md",
    colorScheme: "capri",
  },
  // 3.Add visual variants
  variants: {
    // We cant set borderRadius because of a bug, for now i have built a custom IconButton component to use https://github.com/chakra-ui/chakra-ui/pull/4265/commits/d8b9b23e2e6584e04e87bfca21c50165b62b37f3
    icon: {
      w: "3em",
      px: "12px",
      bg: "lunar.50",
      color: "abyss.800",
      _hover: {
        bg: "lunar.100",
      },
      _focus: {
        bg: "lunar.200",
      },
      _active: {
        bg: "lunar.200",
      },
    },
    solid: (props) => ({
      color: "abyss.800",
      _hover: {
        bg:
          props.colorScheme === "gray"
            ? "gray.100"
            : `${props.colorScheme}.400`,
      },
      _active: {
        bg:
          props.colorScheme === "gray"
            ? "gray.100"
            : `${props.colorScheme}.600`,
      },
      _focus: {
        bg:
          props.colorScheme === "gray"
            ? "gray.100"
            : `${props.colorScheme}.600`,
      },
      _disabled: {
        color: "granite.300",
      },
    }),
    outline: (props) => {
      const { colorScheme: c } = props;
      const borderColor = `gray.200`;
      return {
        border: "2px solid",
        borderColor: c === "gray" ? borderColor : `${c}.500`,
        color: "abyss.800",
        bg: "transparent",
        _hover: {
          bg: "transparent",
          borderColor: c === "gray" ? "gray.100" : `${c}.300`,
        },
        _active: {
          bg: "transparent",
          borderColor: c === "gray" ? "gray.100" : `${c}.600`,
        },
        _disabled: {
          color: "granite.300",
          borderColor: c === "gray" ? "gray.100" : `${c}.300`,
        },
        _focus: {
          borderColor: c === "gray" ? "gray.100" : `${c}.600`,
        },
      };
    },
    link: (props) => {
      const { colorScheme: c } = props;
      return {
        color: c === "abyss" ? "abyss.800" : `${c}.500`,
        _hover: {
          textDecoration: "underline",
          _disabled: {
            textDecoration: "none",
          },
        },
        _active: {
          color: c === "abyss" ? "abyss.800" : `${c}.500`,
        },
      };
    },
    primary: {
      bg: "capri.500",
      _hover: {
        bg: "capri.400",
      },
      _disabled: {
        bg: "capri.300",
      },
      _focus: {
        bg: "capri.600",
      },
      _active: {
        bg: "capri.600",
      },
    },
    secondary: {
      border: "2px solid",
      borderColor: "capri.500",

      _hover: {
        borderColor: "capri.400",
      },
      _disabled: {
        borderColor: "capri.300",
      },
      _focus: {
        borderColor: "capri.600",
      },
      _active: {
        borderColor: "capri.600",
      },
    },
    tertiary: {
      bg: "lunar.50",
      _hover: {
        bg: "lunar.100",
      },
      _focus: {
        bg: "lunar.200",
      },
      _active: {
        bg: "lunar.200",
      },
    },
    textButton: {
      bg: "transparent",
      _hover: {
        bg: "transparent",
      },
      _focus: {
        bg: "transparent",
      },
    },
  },
};
