import React, { ReactElement } from "react";
import { chakra, Box, Flex } from "@chakra-ui/react";
import { TextComponent2Props } from "./TextComponent2Props";

function TextComponent2({
  variant,
  heading,
  text,
  containerProps,
}: TextComponent2Props): ReactElement {
  const color = variant === "light" ? "Arctic" : "Abyss";

  return (
    <Flex
      direction="column"
      sx={{ "> *:last-child": { mb: "0" } }}
      alignItems="flex-start"
      {...containerProps}
    >
      {heading && (
        <chakra.h2
          textStyle={{
            base: `heading6Left${color}`,
          }}
          mb={{ base: "2" }}
          whiteSpace="pre-line"
        >
          {heading}
        </chakra.h2>
      )}
      {text && (
        <Box
          textStyle={{
            base: `paragraph1Left${color}`,
          }}
          mb={{ base: "4", md: "6" }}
          whiteSpace="pre-line"
        >
          {text}
        </Box>
      )}
    </Flex>
  );
}

export default TextComponent2;
