export const splitImageText = {
  fieldGroupName: "SplitImageText",
  textcomponent1Heading: "Split\r\nHeading",
  textcomponent1IncludeButton: true,
  textcomponent1Label: "Split Label",
  textcomponent1Overline: "Split Overline",
  textcomponent1Text: "Split\r\nText",
  textcomponent1ButtonLink: {
    __typename: "AcfLink",
    target: "",
    title: "Home page",
    url: "http://localhost:3000",
  },
  image: {
    __typename: "MediaItem",
    altText: "",
    sourceUrl: "/images/image-1.jpg",
    mediaDetails: {
      __typename: "MediaDetails",
      height: 1074,
      width: 1406,
    },
  },
  textAlignment: "right",
};
