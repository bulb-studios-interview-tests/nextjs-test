import { FlexProps } from "@chakra-ui/react";

export interface TextComponent2Props {
  variant: string;
  heading?: any;
  text?: any;
  containerProps: FlexProps;
}
