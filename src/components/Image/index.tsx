import React from "react";
import NextImage, { ImageProps } from "next/image";

export default function ImageComponent({ ...rest }: ImageProps) {
  return <NextImage {...rest} />;
}
