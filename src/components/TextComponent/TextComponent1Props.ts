import { FlexProps } from "@chakra-ui/react";
import { BadgeProps } from "../Badge/BadgeProps";

export interface TextComponent1Props {
  variant: string;
  heading?: any;
  text?: any;
  label?: string;
  overline?: string;
  buttonLink?: {
    target?: string;
    url: string;
    title: string;
  };
  buttonVariant?: string;
  containerProps?: FlexProps;
  badge?: BadgeProps;
}
