// 1. Import the extendTheme function
import { extendTheme } from "@chakra-ui/react";
import { colors } from "./colors";
import { textStyles } from "./textStyles";
import { fonts } from "./fonts";
import { fontSizes } from "./typography";
import { Button } from "./button";
import { Link } from "./link";
import { FormLabel } from "./formLabel";
import { Input } from "./input";
import { FormError } from "./formError";
import { Textarea } from "./textarea";
import { layerStyles } from "./layerStyles";

// 2. Extend the theme to include custom colors, fonts, etc
export const theme = extendTheme({
  styles: {
    global: (props: any) => {
      return {
        ".WYSIWYG--default": {
          ...textStyles.paragraph1LeftItalicAbyss,
          "h1:not(:first-of-type), * > h1": {
            display: "none",
          },
          h2: {
            ...textStyles.heading6LeftAbyss,
          },
          h3: {
            ...textStyles.heading7LeftAbyss,
          },
          h4: {
            ...textStyles.heading7LeftAbyss,
          },
          strong: {
            fontWeight: 600,
          },
          a: {
            textDecoration: "underline",
          },
          "ul, ol": {
            listStylePosition: "inside",
          },
          blockquote: {
            ...textStyles.heading1LeftAbyss,
          },
        },
      };
    },
  },
  components: {
    Heading: {
      sizes: null,
    },
    Button,
    Link,
    FormLabel,
    Input,
    FormError,
    Textarea,
  },
  fonts,
  fontSizes,
  colors,
  space: {
    spacer: "64px",
  },
  sizes: {
    maxWidth: "1200px",
  },
  config: {
    initialColorMode: "light",
    useSystemColorMode: false,
  },
  textStyles,
  layerStyles,
});

export default theme;
