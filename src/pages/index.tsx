import React from "react";
import { Box } from "@chakra-ui/layout";
import Patterns from "../patterns";
import Layout from "../components/Layout";
import { splitImageText } from "../lib/mockData/splitImageText";

function HomePage({ post }) {
  if (!post) return null;
  return (
    <Layout seo={post.seo}>
      <Box>
        {post.pageLayout.flexibleContent.map((post: any, index: number) => {
          return (
            <Patterns
              fieldGroupName={post.fieldGroupName}
              post={post}
              key={`${post.fieldGroupName}_${index}`}
            />
          );
        })}
      </Box>
    </Layout>
  );
}

export async function getStaticProps() {
  const post = {
    pageLayout: {
      flexibleContent: [splitImageText],
    },
  };
  return { props: { post } };
}

export default HomePage;
