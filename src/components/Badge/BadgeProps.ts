export interface BadgeProps {
  label?: string;
  colorScheme?: string;
  size?: string;
}
