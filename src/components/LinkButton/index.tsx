import React from "react";
import { Link as ChakraLink, Box, chakra } from "@chakra-ui/react";
import { LinkButtonProps } from "./LinkButtonProps";

const LinkButton = React.forwardRef<HTMLAnchorElement, LinkButtonProps>(
  ({ onClick, href, text, rightIcon, leftIcon, ...rest }, ref) => {
    return (
      <ChakraLink href={href} onClick={onClick} ref={ref} {...rest}>
        {leftIcon && (
          <chakra.span
            display="inline-flex"
            alignSelf="center"
            flexShrink={0}
            marginRight="0.5rem"
          >
            {leftIcon}
          </chakra.span>
        )}
        <Box>{text}</Box>
        {rightIcon && (
          <chakra.span
            display="inline-flex"
            alignSelf="center"
            flexShrink={0}
            marginLeft="0.5rem"
          >
            {rightIcon}
          </chakra.span>
        )}
      </ChakraLink>
    );
  }
);

LinkButton.displayName = "LinkButton";

export default LinkButton;
