import React, { ReactElement } from "react";
import { Heading, Box } from "@chakra-ui/react";
import LinkButton from "../../components/LinkButton";
import Link from "next/link";
import { BadgeProps } from "./BadgeProps";

function Badge({ size, label, colorScheme }: BadgeProps): ReactElement {
  return (
    <Box
      textStyle={
        size === "lg"
          ? {
              base: "tagsBadgesTagsWhite",
              md: "tagsBadgesBadgesWhite",
            }
          : {
              base: "tagsBadgesTagsWhite",
            }
      }
      bgColor={colorScheme}
      px={{ base: "4", md: "6" }}
      py="0.5"
      borderRadius="md"
    >
      {label}
    </Box>
  );
}

export default Badge;
