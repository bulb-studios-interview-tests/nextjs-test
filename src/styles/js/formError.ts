import { textStyles } from "./textStyles";

export const FormError = {
  baseStyle: {
    text: { ...textStyles.paragraph3LeftError, mt: 1 },
  },
};
