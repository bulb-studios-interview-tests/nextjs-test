import { ImageProps } from "next/image";
import { TextComponent1Props } from "../../components/TextComponent/TextComponent1Props";

export interface SplitImageTextProps {
  textComponent1: TextComponent1Props;
  image?: ImageProps;
  textAlignment: string;
}
