import React from "react";
import SplitImageText from "./SplitImageText";
import { splitImageTextMapping } from "./SplitImageText/splitImageTextMapping";

type PatternProps = {
  fieldGroupName: string;
  postType?: string;
  post?: any;
  page?: any;
  posts?: any;
};

function Patterns({ fieldGroupName, post }: PatternProps) {
  if (!post) return null;

  try {
    switch (fieldGroupName) {
      case "SplitImageText": {
        const data = splitImageTextMapping(post);
        if (!data) return null;
        return <SplitImageText {...data} />;
      }
      default:
        break;
    }
  } catch (e) {
    console.error(`Issue with pattern: ${fieldGroupName}`);
    console.error(e);
    throw e;
  }

  return null;
}

export default Patterns;
