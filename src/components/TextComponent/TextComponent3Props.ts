import { BoxProps, FlexProps } from "@chakra-ui/react";
import { BadgeProps } from "../Badge/BadgeProps";

export interface TextComponent3Props {
  variant: string;
  heading?: any;
  text?: any;
  buttonLink?: {
    target?: string;
    url: string;
    title: string;
  };
  badge: BadgeProps;
  buttonVariant?: string;
  containerProps: FlexProps;
}
