import { linkResolver } from "../../lib/linkResolver";
import { SplitImageTextProps } from "./splitImageTextProps";

export const splitImageTextMapping = (
  data: any
): SplitImageTextProps | null => {
  if (!data) return null;

  const image = data.image
    ? {
        src: data.image.sourceUrl,
        alt: data.image.altText,
        width: data.image.mediaDetails.width,
        height: data.image.mediaDetails.height,
      }
    : undefined;

  return {
    textComponent1: {
      variant: data.textcomponent1Variant,
      heading: data.textcomponent1Heading,
      text: data.textcomponent1Text,
      label: data.textcomponent1Label,
      buttonLink: data.textcomponent1ButtonLink
        ? {
            ...data.textcomponent1ButtonLink,
            url: linkResolver(data.textcomponent1ButtonLink.url),
          }
        : undefined,
    },
    image,
    textAlignment: data.textAlignment,
  };
};
