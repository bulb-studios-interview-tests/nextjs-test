import React, { ReactElement } from "react";
import { chakra, Box, Flex } from "@chakra-ui/react";
import LinkButton from "../../components/LinkButton";
import Link from "next/link";
import Badge from "../../components/Badge";
import { TextComponent3Props } from "./TextComponent3Props";

function TextComponent3({
  heading,
  text,
  buttonLink,
  buttonVariant,
  variant,
  badge,
  ...containerProps
}: TextComponent3Props): ReactElement {
  const color = variant === "light" ? "Arctic" : "Abyss";

  return (
    <Flex
      direction="column"
      sx={{ "> *:last-child": { mb: "0" } }}
      alignItems="flex-start"
      {...containerProps}
    >
      {badge.label && (
        <Box mb={{ base: "4", md: "5" }}>
          <Badge
            colorScheme="abyss.600"
            size={badge.size}
            label={badge.label}
          />
        </Box>
      )}
      {heading && (
        <chakra.h2
          textStyle={{
            base: `heading5Left${color}`,
            md: `heading4Left${color}`,
          }}
          mb={{ base: "4", md: "6" }}
          whiteSpace="pre-line"
        >
          {heading}
        </chakra.h2>
      )}
      {text && (
        <Box
          textStyle={{
            base: `paragraph1LeftItalic${color}`,
          }}
          mb={{ base: "6", md: "10" }}
          whiteSpace="pre-line"
        >
          {text}
        </Box>
      )}
      {buttonLink && (
        <Box>
          <Link href={buttonLink.url} passHref>
            <LinkButton
              text={buttonLink.title}
              isExternal={buttonLink.target !== "_blank" ? false : true}
              variant={buttonVariant ? buttonVariant : "solid"}
            />
          </Link>
        </Box>
      )}
    </Flex>
  );
}

export default TextComponent3;
