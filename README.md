Hi,

Thank you for applying for the role of frontend develop at Bulb Studios. This is a test that all candidates need to complete in order to proceed to the next stage of the interview.

The test is a stripped down version of our internal pattern library. It’s built using Next.js and typescript using a UI library called [Charka-UI](https://chakra-ui.com). You don’t have to use Chakra-UI but you will need to use styled components. You can import styled components using:

```jsx
import style from "@emotion/styled";
```

Use git appropriately to show your thought process. This will be helpful when we talk through how you implemented this task.

### Task 1:

Create a new pattern called Convincer Banner:

1. Desktop:

![Untitled](desktop.png)

1. Mobile:

![Untitled](mobile.png)

**Please note the following**

- Background color is #edf1f0
- Icons are included in the project
- Don’t need to change font family please use the existing textstyles in the project. They are located at: src/styles/js/textstyles.ts
- The carousel on mobile needs to loop through all the options every 5 seconds. When it reaches the end it needs to start from the beginning. If going in the opposite direction and it reaches the first option it needs to go to the last option.
- Don’t use a third party library to implement carousel. Animation is not important, it doesn’t need to be fancy

### Task2

A contact form is a standard functionality on a cms site. Please create a “Contact Us” page. It will include a form with 3 fields:

- Full Name
- Email
- Message

When the user submits the form it should hit an endpoint. The endpoint will console.log the values and return a success response. The endpoint will also do very basic validation and return the relevant HTTP response if validation fails. It will also console.log the email that it would send the message to.

**Please not the following:**

- You can only use Next.js to implement the endpoint
- The endpoint doesn’t need to actually send the email. It just needs to console.log it
- Styling of the form isn’t important
- The email address that will receive the email cannot be hard-coded

### Quick questions!

Edit the readme and put your answers below.

If you had more time what:

1. would you have liked to do differently?
2. How would you have improved your code
